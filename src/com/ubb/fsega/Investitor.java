package com.ubb.fsega;

public class Investitor {
    private String nume;
    private String prenume;
    private Proprietate proprietate;

    public Investitor(String nume, String prenume, Proprietate proprietate) {
        this.nume = nume;
        this.prenume = prenume;
        this.proprietate = proprietate;
    }

    public Proprietate getProprietate() {
        return proprietate;
    }

    public void setProprietate(Proprietate proprietate) {
        this.proprietate = proprietate;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getPrenume() {
        return prenume;
    }

    public void setPrenume(String prenume) {
        this.prenume = prenume;
    }

    void achizitioneaza(Cartier cartier, Suprafata suprafata) {
        CasaIndividuala casaIndividuala = new CasaIndividuala(suprafata);
    }
}

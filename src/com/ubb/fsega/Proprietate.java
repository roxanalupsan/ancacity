package com.ubb.fsega;

public class Proprietate {
    private int id;
    private CasaIndividuala[] caseIndividuale;

    public Proprietate(int id, CasaIndividuala[] caseIndividuale) {
        this.id = id;
        this.caseIndividuale = caseIndividuale;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CasaIndividuala[] getCaseIndividuale() {
        return caseIndividuale;
    }

    public void setCaseIndividuale(CasaIndividuala[] caseIndividuale) {
        this.caseIndividuale = caseIndividuale;
    }
}

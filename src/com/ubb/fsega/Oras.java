package com.ubb.fsega;

public class Oras {
    private Cartier[] cartiere;

    public Oras(Cartier[] cartiere) {
        this.cartiere = cartiere;
    }

    public Cartier[] getCartiere() {
        return cartiere;
    }

    public void setCartiere(Cartier[] cartiere) {
        this.cartiere = cartiere;
    }


}

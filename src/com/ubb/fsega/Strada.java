package com.ubb.fsega;

public class Strada {
    private String nume;
    private Integer numar;
    private CasaIndividuala[] caseIndividuale;

    public Strada(String nume, Integer numar, CasaIndividuala[] caseIndividuale) {
        this.nume = nume;
        this.numar = numar;
        this.caseIndividuale = caseIndividuale;
    }

    public CasaIndividuala[] getCaseIndividuale() {
        return caseIndividuale;
    }

    public void setCaseIndividuale(CasaIndividuala[] caseIndividuale) {
        this.caseIndividuale = caseIndividuale;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public Integer getNumar() {
        return numar;
    }

    public void setNumar(Integer numar) {
        this.numar = numar;
    }
}

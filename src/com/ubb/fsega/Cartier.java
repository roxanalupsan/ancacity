package com.ubb.fsega;

public class Cartier {
    private String nume;
    private Strada[] strazi;

    public Cartier(String nume, Strada[] strazi) {
        this.nume = nume;
        this.strazi = strazi;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public Strada[] getStrazi() {
        return strazi;
    }

    public void setStrazi(Strada[] strazi) {
        this.strazi = strazi;
    }
}

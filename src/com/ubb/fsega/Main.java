package com.ubb.fsega;

import java.util.Arrays;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        //ordoneaza alfabetic
        String[] names = {"Roxana", "Anca", "Marioara"};
        Arrays.sort(names);
        for (int i = 0; i < names.length; i++)
            System.out.println(names[i]);

        //printeaza numar random
        int[] numbers = {97,88,121,55,66,17,42};
        int randomIndex = new Random().nextInt(numbers.length);
        System.out.println(numbers[randomIndex]);

        //printeaza un String random
        String[] streetName = {"Viilor", "Brancusi", "Borhanci", "Groza"};
        int index = new Random().nextInt(streetName.length);
        System.out.println(streetName[index]);
    }
}

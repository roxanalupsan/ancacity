package com.ubb.fsega;

public class CasaIndividuala {
    private Suprafata suprafata;

    public CasaIndividuala(Suprafata suprafata) {
        this.suprafata = suprafata;
    }

    public Suprafata getSuprafata() {
        return suprafata;
    }

    public void setSuprafata(Suprafata suprafata) {
        this.suprafata = suprafata;
    }
}
